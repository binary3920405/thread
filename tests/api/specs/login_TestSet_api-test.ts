import { checkResponseTime, checkStatusCode } from '../../helpers/functionsForChecking.helper';
import { AuthController } from '../lib/controllers/auth.controller';

const auth = new AuthController();

describe('Use test data set for login', () => {
    let invalidCredentialsDataSet = [
        { email: 'test_user2@example.com', password: 'STRONGPASSWORD*' },
        { email: 'test_user2@example.com', password: '    ' },
        { email: 'test_user2@example.com', password: 'test_user2@example.com' },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`should not login using invalid credentials: '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await auth.login(credentials.email, credentials.password);

            checkStatusCode(response, 401);
            checkResponseTime(response, 3000);
        });
    });

    let validCredentialsDataSet = [
        { email: 'test_user4@example.com', password: 'Stro' },
        { email: 'test_user3@examp.le.com', password: 'StrongPassword12' },
        { email: 'test_user2@example.com', password: 'StrongPassword*' },
    ];

    validCredentialsDataSet.forEach((credentials) => {
        it(`should login using valid credentials: '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await auth.login(credentials.email, credentials.password);

            checkStatusCode(response, 200);
            checkResponseTime(response, 3000);
        
        });
    });
});