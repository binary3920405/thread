import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { RegistrationController } from "../lib/controllers/registration.controller";
import { CommentsController, CreatePostController, ReactionsController, PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new PostsController();
const auth = new AuthController();
const registration = new RegistrationController();
const commentsController = new CommentsController();
const createPostController = new CreatePostController();
const reactionsController = new ReactionsController();
const postsController = new PostsController();  

let accessToken: string;
let userId: number;
let postId: number;

describe("some post", () => {
    describe("Registration with valid data", () => {
        it(`Successfully register a new user`, async () => {
            let response = await registration.register(
                "no avatar",
                "shliakhta111@gmail.com",
                "olenochka",
                "Sob777sob*"
            );

            userId = response.body.user.id;
        });
    });

    describe("Actions with post", () => {
        it(`Authentication with valid credentials`, async () => {
            let response = await auth.login("shliakhta111@gmail.com", "Sob777sob*");

            accessToken = response.body.token.accessToken.token;
        });

        it(`Get all posts`, async () => {
            let response = await postsController.getAllPosts();
        
            checkStatusCode(response, 200);
            checkResponseTime(response, 3000);
        });

        it("Add new post", async () => {
           
            let postPayload = {
                authorId: userId,
                previewImage: "testImage.jpg",
                body: "My new post"
            };

            let createPostResponse = await createPostController.createNewPost(postPayload, accessToken);

            postId = createPostResponse.body.id;

            checkStatusCode(createPostResponse, 200);
            checkResponseTime(createPostResponse, 3000);
            
        });

        it("Add new comment", async () => {
            
            let commentPayload = {
                authorId: userId,
                postId: postId,
                body: "My new comment"
            };

            let addCommentResponse = await commentsController.addComment(commentPayload, accessToken);

            checkStatusCode(addCommentResponse, 200);
            checkResponseTime(addCommentResponse, 3000);
            
        });

        it("Add new like", async () => {
        
            let likePayload = {
                entityId: postId,
                isLike: true,
                userId: userId
            };

            let addLikeResponse = await reactionsController.likePost(likePayload, accessToken);

            checkStatusCode(addLikeResponse, 200);
            checkResponseTime(addLikeResponse, 3000);
            
        });
    });
});
