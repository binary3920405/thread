import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { RegistrationController } from "../lib/controllers/registration.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();
const registration = new RegistrationController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));
let accessToken: string;
let userId: number;

describe("Registration, Authentication, userByToken, Update, userById, Delete ", () => {
    describe("Registration with valid data", () => {
        it(`Successfully register a new user`, async () => {
            let response = await registration.register(
                "no avatar",
                "shliakhta2@gmail.com",
                "olenochka",
                "Sob777sob*"
            );

            checkStatusCode(response, 201);
            checkResponseTime(response, 3000);

            userId = response.body.user.id;
        });
    });

    describe(`Get all users `, () => {
        let userId: number;
     
        it(`Return 200 status code and all users when getting the user collection`, async () => {
            let response = await users.getAllUsers();
    
            checkStatusCode(response, 200);
            checkResponseTime(response,3000);
            expect(response.body.length, `Response body has more than 1 item`).to.be.greaterThan(1); 
            expect(response.body).to.be.jsonSchema(schemas.schema_allUsers); 
            
            userId = response.body[1].id;
        });
    });

    describe("Authenticated actions", () => {
        it(`Authentication with valid credentials`, async () => {
            let response = await auth.login("shliakhta2@gmail.com", "Sob777sob*");

            checkStatusCode(response, 200);
            checkResponseTime(response, 3000);

            accessToken = response.body.token.accessToken.token;
        });

        it("Return current user using the obtained token", async () => {
            let response = await users.getCurrentUser(accessToken);

            checkStatusCode(response, 200);
            checkResponseTime(response, 3000);

            expect(response.body.email).to.equal("shliakhta2@gmail.com");
        });

        it("Update user", async () => {
            let userData: object = {
                id: userId,
                avatar: "no avatar",
                email: "shliakhta2@gmail.com",
                userName: "OlenaUpdated",
            };

            let updateResponse = await users.updateUser(userData, accessToken);

            checkStatusCode(updateResponse, 204);
            checkResponseTime(updateResponse, 3000);
        });

        it("Check updated body by token", async () => {
            let response = await users.getCurrentUser(accessToken);

            checkStatusCode(response, 200);
            checkResponseTime(response, 3000);

            expect(response.body.userName).to.equal("OlenaUpdated");
        });

        it("Get user by ID", async () => {
            let getUserResponse = await users.getUserById(userId);

            checkStatusCode(getUserResponse, 200);
            checkResponseTime(getUserResponse, 3000);

            expect(getUserResponse.body.id).to.equal(userId);
        });

        it("Delete user by ID", async () => {
            let deleteUserResponse = await users.deleteUser(userId, {}, accessToken);

            checkStatusCode(deleteUserResponse, 204);
            checkResponseTime(deleteUserResponse, 3000);
        });

    });
});
