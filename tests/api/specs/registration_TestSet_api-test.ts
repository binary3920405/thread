import { expect } from 'chai';
import { RegistrationController } from '../lib/controllers/registration.controller';
import { checkStatusCode, checkResponseTime } from '../../helpers/functionsForChecking.helper';

const registration = new RegistrationController();

describe('Valid test data set for registration', () => {
    let validRegistrationDataSet = [
        { avatar: 'no avatar', email: 'test_user1@example.com', userName: 'testuser1', password: 'StrongPassword1' },
        { avatar: '', email: 'test_user2@example.com', userName: 'testuser2', password: 'StrongPassword*' },
        { avatar: 'no avatar', email: 'test_user3@examp.le.com', userName: 'use', password: 'StrongPassword12' },
        { avatar: 'no avatar', email: 'test_user4@example.com', userName: 'юзер', password: 'Stro' },
    ];

    validRegistrationDataSet.forEach((data) => {
        it(`should register using valid data: ${JSON.stringify(data)}`, async () => {
            let response = await registration.register(data.avatar, data.email, data.userName, data.password);

            checkStatusCode(response, 201); 
            checkResponseTime(response, 3000);
            expect(response.body.user.id).to.exist;
        });
    });
});

describe('Invalid test data set for registration', () => {
    let invalidRegistrationDataSet = [
        { avatar: '', email: '', userName: '', password: '' },
        { avatar: 'no avatar', email: '', userName: 'testuser', password: 'Password123' },
        { avatar: 'no avatar', email: '@example.com', userName:"username", password: 'Password123' }
    ];

    invalidRegistrationDataSet.forEach((data) => {
        it(`should not register using invalid data: ${JSON.stringify(data)}`, async () => {
            let response = await registration.register(data.avatar, data.email, data.userName, data.password);

            checkStatusCode(response, 400); 
            checkResponseTime(response, 3000);

        });
    });
});
