import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class CommentsController {
    async addComment(commentData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Comments`)
            .body(commentData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}

export class PostsController {
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }
}
export class CreatePostController {
    async createNewPost(postData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body(postData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}

export class ReactionsController {
    async likePost(reactionData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body(reactionData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
